import math
class Drawing:
        def __init__(self):
            self.vector=[]
            self.value=0

def fun():
    return Drawing()


class Distance:
    def __init__(self):
        self.euklidesz = 0
        self.value = 0

def fun2():
    return Distance()


#----------------------------------------------------------------------------------------
#knn

def euklidesz(a,b):
    suma = 0
    for i in range(64):
        suma = suma + (a[i] - b[i]) ** 2
    return math.sqrt(suma)

def sort_distance_by_euklidesz(distance):
    return distance.euklidesz

def knn(vector, training, k):
    distances = []
    for i in range(len(training)):
        distance = fun2()
        distance.euklidesz = euklidesz(vector, training[i].vector)
        distance.value = training[i].value
        distances.append(distance)
    distances.sort(key=sort_distance_by_euklidesz)
    elofordulas = []
    
    for i in range(10):
        elofordulas.append(0)

    for i in range(k):
        #print(distances[i].euklidesz, distances[i].value)
        elofordulas[int(distances[i].value)] = elofordulas[int(distances[i].value)] + 1
    
    maxi = 0
    maxi_index = 0
    for i in range(10):
        if elofordulas[i] > maxi:
            maxi = elofordulas[i]
            maxi_index = i
    return maxi_index
    

#----------------------------------------------------------------------------------------------
#centroid

def create_prototype(training):
    prototypes = []
    nr = []
    for i in range(10):
        prototype = []
        for j in range(64):
            prototype.append(0)
        prototypes.append(prototype)
        nr.append(0)
    
    for i in range(len(training)): 
        index = int(training[i].value)
        for j in range(64):
            prototypes[index][j] = prototypes[index][j] + training[i].vector[j]
        nr[index] = nr[index] + 1

    for i in range(10):
        for j in range(64):
            prototypes[i][j] = prototypes[i][j] / nr[i]

    return prototypes

def centroid(vector, training):
    tabla = create_prototype(training) 
    mini = 100
    mini_index = 0
    for i in range(10):
        dist = euklidesz(tabla[i],vector)
        if dist < mini:
            mini = dist 
            mini_index = i
    return mini_index

#---------------------------------------------------------------------------------------
#Tanulasi hiba

def tanulasi_hiba(training, method):
    mistakes = []
    nr = []
    for i in range(10):
        mistakes.append(0)
        nr.append(0)
    for i in range(len(training)):
        if method:
            value = knn(training[i].vector, training, 5)
        else:
            value = centroid(training[i].vector, training)
        
        nr[int(training[i].value)] = nr[int(training[i].value)] + 1
        if int(training[i].value) != value:
            mistakes[int(training[i].value)] = mistakes[int(training[i].value)] + 1
    for i in range(10):
        mistakes[i] = mistakes[i] / nr[i]

    if method:
        print("Tanulasi hiba knn el: ")
    else:
        print("Tanulasi hiba centroiddal: ")
    
    for i in range(10):
        print(i,  mistakes[i])

#---------------------------------------------------------------------------------------
#Teszt hiba

def teszt_hiba(test, results, training, method):
    mistakes = []
    nr = []
    for i in range(10):
        mistakes.append(0)
        nr.append(0)
    for i in range(len(test)):
        if method:
            value = knn(test[i], training, 5)
        else:
            value = centroid(test[i], training)
        
        nr[results[i]] = nr[results[i]] + 1
        if results[i] != value:
            mistakes[results[i]] = mistakes[results[i]] + 1
    for i in range(10):
        mistakes[i] = mistakes[i] / nr[i]

    if method:
        print("Teszt hiba knn el: ")
    else:
        print("Teszt hiba centroiddal: ")
    
    for i in range(10):
        print(i,  mistakes[i])


#----------------------------------------------------------------------------------------
#Gradiens

def gradiesns(training, a, b):
    if b < a:
        seged = b
        b = a
        a = seged
    x = []
    y = []
    for i in range(len(training)):
        if int(training[i].value) == a or int(training[i].value) == b:
            xi = []
            for j in range(len(training[i].vector)):
                xi.append(training[i].vector[j])
            if int(training[i].value) == a:
                y.append(-1)
            else:
                y.append(1)
            xi.append(1)
            x.append(xi)
    y.append(1)


    l = len(x)
    w = []
    for i in range(65):
        w.append(1)

    osszeg = 0
    gamma = 0.5

    
    # for i in range(1000):
    #     uj = []
    #     osszeg = 0
    #     for j in range(65):
    #         for k in range(64):
    #             szam  = 2 * x[j][k] * x[j][k] * w[j] - 2 * x[j][k] * b
    #             #negyzet = x[j][k] * x[j][k]
    #             osszeg = osszeg + szam
    #     for j in range(64):
    #         w[i+1] = w[i] - gamma * osszeg

    # sum((w*xi - yi) ^ 2)
    #derivalt: 2 * xi^2 * w - 2 * xi * yi
    for i in range(1000):
        
        for j in range(65):
            osszeg =[]
            szam = []
            for k in range(64):
                osszeg.append(0)
                szam.append(0)
            for k in range(64):
                szam[k]  = 2 * x[j][k] * x[j][k] * w[j] - 2 * x[j][k] * y[j]
            for k in range(len(szam)):
                osszeg[k] = osszeg[k] + szam[k]
        for j in range(64):
            w[j] = w[j] - gamma * osszeg[j]
        

    return w


def ez_vagy_az(vector, a, b, training):
    vector.append(1)
    w = gradiesns(training, a , b)
    egyenes = 0
    print(len(w))
    print(len(vector))
    for i in range(len(vector)):
        egyenes = egyenes + w[i] * vector[i]
    print(egyenes)
    if egyenes < 0:
        return a
    else:
        return b
            
            
            


def print_tabla(tabla,n):
    print("")
    szamlalo = 0
    for i in range(n):
        for j in range(n):
            print(tabla[szamlalo] , end = "  ")
            szamlalo = szamlalo + 1
        print("")
    print("")

if __name__ == '__main__':
    training = []
    test = []
    with open("C:/Egyetem info/4. felev/AI/Hazi/H3/optdigits.tra") as f:
        for row in f:
            drawing = fun()
            data = row.strip().split(",")
            for j in range(len(data)-1):
                drawing.vector.append(int(data[j]))
            drawing.value = data[64]
            training.append(drawing)

    results = []
    with open("C:/Egyetem info/4. felev/AI/Hazi/H3/optdigits.tes") as f:
        for row in f:
            data = row.strip().split(",")
            entitans = []
            for j in range(len(data)-1):
                entitans.append(int(data[j]))
            test.append(entitans)
            results.append(int(data[64]))


    # k = 5
    # result_knn = []
    # for i in range(10):
    #     result_knn.append(0)

    # for i in range(len(test)):
    #     value = knn(test[i], training, k)
    #     result_knn[value] = result_knn[value] + 1
    #     #print(i)
    
    # for i in range(10):
    #     print(i, result_knn[i])

    # result_centroid = []
    # for i in range(10):
    #     result_centroid.append(0)

    # for i in range(len(test)):
    #     value = centroid(test[i], training)
    #     result_centroid[value] = result_centroid[value] + 1
    #     print(i)
    
    # for i in range(10):
    #     print(i, result_centroid[i])

    # tanulasi_hiba(training, 1)
    # tanulasi_hiba(training, 0)
    # teszt_hiba(test, results, training, 1)
    # teszt_hiba(test, results, training, 0)

    w = gradiesns(training, 4, 7)
    print(w)

    print(ez_vagy_az(training[10].vector, 4, 7, training))

    

