#Szasz Csongor
#523/II
#scim1957

import sys
import numpy as np
from copy import copy, deepcopy
import tkinter

def keres_poz(i,j,tabla,n):
    if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:
        return False
    if i-1>=0 and tabla[i-1][j]==0:
        return False
    if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
        return False
    if j-1>=0 and tabla[i][j-1]==0:
        return False
    if j+1<n and tabla[i][j+1]==0:
        return False
    if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
        return False
    if i+1<n and tabla[i+1][j]==0:
        return False
    if i+1<n and j+1<n and tabla[i+1][j+1]==0:
        return False
    return True

def nyert_e(tabla,n,soros):
    if(soros==True):
        for i in range(n):
            for j in range(n):
                if (tabla[i][j]==2):
                    mentes_i=i
                    mentes_j=j
        if(keres_poz(mentes_i,mentes_j,tabla,n)):
            return -1
        else:
            return 0
    else:
        for i in range(n):
            for j in range(n):
                if (tabla[i][j]==3):
                    mentes_i=i
                    mentes_j=j
        if(keres_poz(mentes_i,mentes_j,tabla,n)):
            return 1
        else:
            return 0


class Lepes:
    def __init__(self):
        self.tabla=[[0]]
        self.ertek=-2

def fun():
    return Lepes()

def print_tabla(tabla,n):
    print("")
    for i in range(n):
        for j in range(n):
            print(tabla[i][j] , end = " ")
        print("")
    print("")

def minimax(tabla,n,melyseg,maximizingPlayer):
    lepes = fun()
    #lepes.tabla=tabla
    eva = []
    palyak = []
    szamlalo=0
    if maximizingPlayer:                                #A BOT jatekos a soros
        node_value = nyert_e(tabla,n,maximizingPlayer)
        if node_value!=0:
            lepes.ertek = node_value/melyseg
            lepes.tabla=tabla
            #print_tabla(tabla,3)
            return lepes
        
        for i in range(n):                  #Megkeresem a BOT aktualis poziciojat
            for j in range(n):
                if (tabla[i][j]==2):
                    mentes_i=i
                    mentes_j=j
        
        i=mentes_i
        j=mentes_j

        if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:                #Kovetkezo lepes
            tabla[i][j] = 0
            tabla[i-1][j-1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        mas=tabla
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i-1][j-1] = 0
        if i-1>=0 and tabla[i-1][j]==0:
            tabla[i][j] = 0
            tabla[i-1][j] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        mas=tabla
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0 
            tabla[i][j] = 2
            tabla[i-1][j] = 0
        if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
            tabla[i][j] = 0
            tabla[i-1][j+1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        mas=tabla
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0 
            tabla[i][j] = 2
            tabla[i-1][j+1] = 0
        if j-1>=0 and tabla[i][j-1]==0:
            tabla[i][j] = 0
            tabla[i][j-1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        #print_tabla(tabla,3)
                        mas=tabla
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i][j-1] = 0
        if j+1<n and tabla[i][j+1]==0:
            tabla[i][j] = 0
            tabla[i][j+1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        #print_tabla(tabla,3)
                        mas=tabla
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i][j+1] = 0
        if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
            tabla[i][j] = 0
            tabla[i+1][j-1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        mas=tabla
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i+1][j-1] = 0
        if i+1<n and tabla[i+1][j]==0:
            tabla[i][j] = 0
            tabla[i+1][j] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        #print_tabla(tabla,3)
                        mas=tabla
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i+1][j] = 0
        if i+1<n and j+1<n and tabla[i+1][j+1]==0:
            tabla[i][j] = 0
            tabla[i+1][j+1] = 2
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #if melyseg==0:
                        #    print_tabla(tabla,3)
                        #print_tabla(tabla,3)
                        mas=tabla
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,False))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 2
            tabla[i+1][j+1] = 0

        maxi_eva = fun()
        maxi_eva.ertek=eva[0].ertek
        maxi_eva.tabla=palyak[0]
        for i in range(szamlalo):
            if eva[i].ertek > maxi_eva.ertek:
                maxi_eva.ertek=eva[i].ertek
                maxi_eva.tabla=palyak[i]
        
        return maxi_eva

    else:                                               #Player a soros
        node_value = nyert_e(tabla,n,maximizingPlayer)
        if node_value!=0:
            lepes.ertek = node_value/melyseg
            lepes.tabla=tabla
            #print_tabla(tabla,n)
            return lepes
        
        for i in range(n):                  #Megkeresem a Player aktualis poziciojat
            for j in range(n):
                if (tabla[i][j]==3):
                    mentes_i=i
                    mentes_j=j
        
        i=mentes_i
        j=mentes_j

        if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:                #Kovetkezo lepes
            tabla[i][j] = 0
            tabla[i-1][j-1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i-1][j-1] = 0
        if i-1>=0 and tabla[i-1][j]==0:
            tabla[i][j] = 0
            tabla[i-1][j] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0 
            tabla[i][j] = 3
            tabla[i-1][j] = 0
        if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
            tabla[i][j] = 0
            tabla[i-1][j+1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0 
            tabla[i][j] = 3
            tabla[i-1][j+1] = 0
        if j-1>=0 and tabla[i][j-1]==0:
            tabla[i][j] = 0
            tabla[i][j-1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i][j-1] = 0
        if j+1<n and tabla[i][j+1]==0:
            tabla[i][j] = 0
            tabla[i][j+1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i][j+1] = 0
        if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
            tabla[i][j] = 0
            tabla[i+1][j-1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i+1][j-1] = 0
        if i+1<n and tabla[i+1][j]==0:
            tabla[i][j] = 0
            tabla[i+1][j] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i+1][j] = 0
        if i+1<n and j+1<n and tabla[i+1][j+1]==0:
            tabla[i][j] = 0
            tabla[i+1][j+1] = 3
            for k in range(n):
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        #print_tabla(tabla,3)
                        palyak.append(deepcopy(tabla))
                        eva.append(minimax(tabla,n,melyseg+1,True))
                        szamlalo = szamlalo+1
                        tabla[k][l] = 0
            tabla[i][j] = 3
            tabla[i+1][j+1] = 0

        mini_eva = fun()
        mini_eva.ertek=eva[0].ertek
        mini_eva.tabla=palyak[0]
        for i in range(szamlalo):
            if eva[i].ertek < mini_eva.ertek:
                mini_eva.ertek=eva[i].ertek
                mini_eva.tabla=palyak[i]
        return mini_eva


global lepes_szamlalo
lepes_szamlalo = 0


def push(i,j,tabla,n,button):
    global lepes_szamlalo
    if(tabla[i][j]!=0):
        print("Nem lehet oda lepni")
        return
    if lepes_szamlalo%2==0:
        for k in range(n):
            for l in range(n):
                if(tabla[k][l] == 3):
                    regi_i = k
                    regi_j = l
        if abs(regi_i-i)>=2 or abs(regi_j-j)>=2:
            print("Nem lehet oda lepni")
            return
        tabla[i][j] = 3
        tabla[regi_i][regi_j] = 0
        draw(tabla,button,n)
    else:
        tabla[i][j]=1
        draw(tabla,button,n)
        if nyert_e(tabla,n,True)!=0:
            print("Nyertel :)")
            return
        kov = fun()
        kov = minimax(tabla,n,0,True)
        #print_tabla(kov.tabla,3)
        for v in range(n):
            for b in range(n):
                tabla[v][b]=kov.tabla[v][b]
        print(kov.ertek)
        draw(tabla,button,n)
        if nyert_e(tabla,n,False)!=0:
            print("Vesztettel :(")
            return
    #print(i)
    #print(j)
    lepes_szamlalo=lepes_szamlalo+1
    #print(lepes_szamlalo)

def draw(tabla,button,n):
    for i in range(n):
        for j in range(n):
            if tabla[i][j]==0:
                button[i][j].config(bg="white")
            elif tabla[i][j] == 1:
                button[i][j].config(bg="grey")
            elif tabla[i][j] == 2:
                button[i][j].config(bg="red")
            elif tabla[i][j] == 3:
                button[i][j].config(bg="green")



# Call main function
if __name__ == '__main__':
    tabla = [[0,2,0],
    [0,0,0],
    [0,3,0]]
    #mas=tabla
    n=3
    
    bot_kezd = False

    if bot_kezd:
        tabla = [[0,0,0],[2,1,0],[0,3,0]]

    master=tkinter.Tk()
    master.title("Kutyaszorito")
    master.geometry("250x270")

    button = [ [ 0 for i in range(n) ] for j in range(n) ]
    for i in range(0,n):
        for j in range(0,n):   
            button[i][j]=tkinter.Button(master,command=lambda  i=i, j=j:push(i,j,tabla,n,button))
            button[i][j].grid(row=i,column=j)
            button[i][j].config(width=10, height=5)
            button[i][j].config(bg="white")

    draw(tabla,button,n)

    master.mainloop()

    #print(tabla[0][2])