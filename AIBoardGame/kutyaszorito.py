import tkinter
import numpy as np
import time

#piros - bot, zold -en
#szinek: 0 - feher (szabad), 1 - fekete (lezart), 2 - piros, 3 - zold
szinek=((0,"feher"),(1,"fekete"),(2,"piros"),(3,"zold"))

global hanyadikLepes
hanyadikLepes = 0

tabla = [ [ 0 for i in range(3) ] for j in range(3) ]
tabla[0][1] = 2
tabla[2][1] = 3

# 1 - lep, 2 - kizar
mitCsinal = 1
# 1 - zold, 2 - piros
kiJon = 1

elozoCellaPiros = [0 for i in range(0,2)]
elozoCellaPiros[0] = 0
elozoCellaPiros[1] = 1
elozoCellaZold = [0 for i in range(0,2)]
elozoCellaZold[0] = 2
elozoCellaZold[1] = 1

master=tkinter.Tk()
master.title("grid() method")
master.geometry("500x400")

whosNext = tkinter.Label(text="zold lep")
whosNext.grid(row=4,column=0)
whosNext.config(width=10, height=5)

#gombok inic
szamlalo=0
button = [ [ 0 for i in range(3) ] for j in range(3) ]

def holVanPiros(tabla,piros):
    for i in range(0,3):
        for j in range(0,3):
            if (tabla[i][j] == 2):
                piros[0] = i
                piros[1] = j

def holVanZold(tabla,zold):
    for i in range(0,3):
        for j in range(0,3):
            if (tabla[i][j] == 3):
               zold[0] = i
               zold[1] = j

def holVanJatekos(tabla,ki):
    poz = [0 for i in range(0,2)]
    for i in range(0,3):
        for j in range(0,3):
            if (tabla[i][j] == ki):
               poz[0] = i
               poz[1] = j
    return poz

#def mennyiPontotKap(tabla,ki):
    #nem tudom
            

def allas(tabla,melyikJatekos):    #vege-e a jeteknak, nyert-e valaki: 0 - nincs vege, 2 - bot nyert, 3 - ember nyert
    #print("tabla allasa, ",melyikJatekos," lepne")
    #print(np.matrix(tabla))
    k = 3

    lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]

    piros = [0 for i in range(0,2)]
    zold = [0 for i in range(0,2)]
    
    holVanPiros(tabla,piros)
    holVanZold(tabla,zold)

    #print("piros koord: ",piros[0],piros[1])
    #print("zold koord: ",zold[0], zold[1])

    tudLepniPiros = 0
    tudLepniZold = 0

    for i in lepes:
        xP = piros[0] + i[0]
        yP = piros[1] + i[1]

        if xP > k-1 or xP < 0:
            continue
        if yP > k-1 or yP < 0:
            continue
        if tabla[xP][yP] == 0:
            tudLepniPiros = 1

    for i in lepes:
        xZ = zold[0] + i[0]
        yZ = zold[1] + i[1]
        
        #print("xz yz ",xZ,yZ)
        if xZ > k-1 or xZ < 0:
            continue
        if yZ > k-1 or yZ < 0:
            continue
        #print("xz yz tabla[xz][yz]=",xZ,yZ,tabla[xZ][yZ])
        
        if tabla[xZ][yZ] == 0:
         #   print("xz yz tabla[xz][yz]=",xZ,yZ,tabla[xZ][yZ])
            tudLepniZold = 1

    #print("Tud-e lepni piros: ", tudLepniPiros)
    #print("Tud-e lepni zold: ", tudLepniZold)

    if tudLepniZold == 0:
        return 10
    if tudLepniPiros == 0:
        return -10
    return 0

def minMaxLepes(tabla,melyseg,melyikJatekos):
    #print('minMaxLepesben melyikJatekos ',melyikJatekos)
    pont = allas(tabla, melyikJatekos)

    if pont == 10:
        #print('piros nyert')
        return 10
    if pont == -10:
        #print('zold nyert')
        return -10

    #print('kezdodik a minimax')
    #print(melyikJatekos)    #bot #2-piros, 3-zold
    #print(tabla)

    if melyikJatekos == 2:              #maximizer
        legjobb=-1000

        lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]

        jatekos = [0 for i in range(0,2)]
        jatekos = holVanJatekos(tabla,melyikJatekos)

        #print("jatekos koord: ",jatekos[0],jatekos[1])

        for i in lepes:
            #koord_poz = (piros[0] + i[0],piros[1] + i[1])
            x = jatekos[0] + i[0]
            y = jatekos[1] + i[1]

            if x > 2 or x < 0:
                continue
            if y > 2 or y < 0:
                continue
            if tabla[x][y] == 0:
                tabla[x][y] = melyikJatekos
                tabla[jatekos[0]][jatekos[1]] = 0
                ujJatekos = melyikJatekos
                if(melyikJatekos == 2):
                    ujJatekos = 3
                else:
                    ujJatekos = 2 
                legjobb = max(legjobb, minMaxKizar(tabla, melyseg+1,ujJatekos))
                #----------------visszatenni az elozo jatekost ----------------
                tabla[x][y] = 0
                tabla[jatekos[0]][jatekos[1]] = melyikJatekos
      #  print('hova tud lepni')
      #  print(hovaTudLepni)
        #print('min max lepes legjobb ',melyikJatekos,' jatekosnak')
        return legjobb

    else:
        legjobb=1000
        lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]
        jatekos = [0 for i in range(0,2)]
        
        jatekos = holVanJatekos(tabla,melyikJatekos)

        for i in lepes:
            #koord_poz = (piros[0] + i[0],piros[1] + i[1])
            x = jatekos[0] + i[0]
            y = jatekos[1] + i[1]

            if x > 2 or x < 0:
                continue
            if y > 2 or y < 0:
                continue
            if tabla[x][y] == 0:
                tabla[x][y] = melyikJatekos
                tabla[jatekos[0]][jatekos[1]] = 0
                if(melyikJatekos == 2):
                    ujJatekos = 3
                else:
                    ujJatekos = 2
                legjobb = min(legjobb, minMaxKizar(tabla, melyseg+1,ujJatekos))
                tabla[x][y] = 0
                tabla[jatekos[0]][jatekos[1]] = melyikJatekos

        #print('min max lepes legjobb ',melyikJatekos,' jatekosnak')
        return legjobb

def minMaxKizar(tabla,melyseg,melyikJatekos):
    #print('minMaxKizarban melyikJatekos ',melyikJatekos) 
    pont = allas(tabla, melyikJatekos)

    if pont == 1:
       # print('piros nyert')
        return 1
    if pont == -1:
       # print('zold nyert')
        return -1

    #print('kezdodik a minimax')
   # print(melyikJatekos)    #bot #2-piros, 3-zold
    #print(tabla)

    if melyikJatekos == 2:
        legjobb=-1000

       # lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]
        lepes = []

        for i in range(0,3):
            for j in range(0,3):
                if tabla[i][j] == 0:
                    lepes.append((i,j))

        jatekos = [0 for i in range(0,2)]
        jatekos = holVanJatekos(tabla,melyikJatekos)

       # print("jatekos koord: ",jatekos[0],jatekos[1])

        #hovaTudLepni = []

        for i in lepes:
            #koord_poz = (piros[0] + i[0],piros[1] + i[1])
            x = i[0]
            y = i[1]
            #print('kizar i[0] i[1] ', i[0],i[1])

            if x > 2 or x < 0:
                continue
            if y > 2 or y < 0:
                continue
            if tabla[x][y] == 0:
                tabla[x][y] = 1
                #hovaTudLepni.append((x,y))
                ujJatekos = melyikJatekos
                if(melyikJatekos == 2):
                    ujJatekos = 3
                else:
                    ujJatekos = 2
                legjobb = max(legjobb, minMaxLepes(tabla, melyseg+1,ujJatekos))
                tabla[x][y] = 0
      #  print('hova tud lepni')
      #  print(hovaTudLepni)
        return legjobb

    else:
        #print('3as jatekos')
        legjobb=1000

        lepes = []

        for i in range(0,3):
            for j in range(0,3):
                if tabla[i][j] == 0:
                    lepes.append((i,j))
        jatekos = [0 for i in range(0,2)]
        jatekos = holVanJatekos(tabla,melyikJatekos)
        #print("jatekos koord2: ",jatekos[0],jatekos[1])
        for i in lepes:
            #koord_poz = (piros[0] + i[0],piros[1] + i[1])
            x = i[0]
            y = i[1]
            #print('kizar i[0] i[1] ', i[0],i[1])

            if x > 2 or x < 0:
                continue
            if y > 2 or y < 0:
                continue
            if tabla[x][y] == 0:
                tabla[x][y] = 1
                ujJatekos = melyikJatekos
                if(melyikJatekos == 2):
                    ujJatekos = 3
                else:
                    ujJatekos = 2
                legjobb = min(legjobb, minMaxLepes(tabla, melyseg+1,ujJatekos))
                tabla[x][y] = 0
      #  print('hova tud lepni')
      #  print(hovaTudLepni)
        return legjobb

def findBestMove(tabla,ki):
    bestVal = -1000
    bestMove = [0 for i in range(0,2)] 
    bestMove[0] = -1
    bestMove[1] = -1 

    piros = [0 for i in range(0,2)]
    piros[0] = -1
    piros[1] = -1
    #piros = holVanJatekos(tabla,2)
    piros = holVanJatekos(tabla,ki)
    #print(np.matrix(tabla))
    #print('best moveban ki, koord ',ki,piros[0],piros[1])
    lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]

    for i in lepes:
        x = piros[0] + i[0]
        y = piros[1] + i[1]
       # print('belep for')

        if x > 2 or x < 0:
            continue
        if y > 2 or y < 0:
            continue
       # print('x,y,tabla[x][y]',x, y, tabla[x][y])
        if tabla[x][y] == 0:
            #print('belep if')
            tabla[x][y] = ki #odalep
            tabla[piros[0]][piros[1]] = 0
            moveVal = minMaxLepes(tabla,0,3)        #-----------------------------itt miert a 3-asra hivom meg?-------------------------------------------------
            #print('x,y, moveVal ',x,y,moveVal)
            tabla[x][y] = 0
            tabla[piros[0]][piros[1]] = ki
            
            if moveVal > bestVal :
                bestMove[0] = x
                bestMove[1] = y
                bestVal = moveVal
               # continue   #

    #print('best move is ',bestMove,'ennek ',ki)

    if(bestMove[0] == -1):
       # print('nem ',ki,'nyert, hanem a masik')
        whosNext.grid(row=4,column=0)
        whosNext.config(width=10, height=5)
        if ki == 2:
            whosNext["text"]="zold nyert"
        else:
            whosNext["text"]="piros nyert"
        bestMove[0] = 0
        bestMove[1] = 0
    else:
        return bestMove

def findBestLock(tabla,melyikJatekos):
    bestVal = -1000
    bestLock = [0 for i in range(0,2)] 
    bestLock[0] = -1
    bestLock[1] = -1 

    piros = [0 for i in range(0,2)]
    piros = holVanJatekos(tabla,2)
   # print('best moveban ki, koord ',melyikJatekos,piros[0],piros[1])
    lepes = [(-1,0),(0,-1),(1,0),(0,1),(-1,-1),(-1,1),(1,-1),(1,1)]

    for i in lepes:
            x = piros[0] + i[0]
            y = piros[1] + i[1]

            if x > 2 or x < 0:
                continue
            if y > 2 or y < 0:
                continue
            if tabla[x][y] == 0:
                tabla[x][y] = melyikJatekos # odalep
                tabla[piros[0]][piros[1]] = 0
                moveVal = minMaxKizar(tabla,0,3)
                tabla[x][y] = 0
                tabla[piros[0]][piros[1]] = melyikJatekos
                
                if moveVal > bestVal :
                    bestLock = (x,y)
                    bestVal = moveVal
                  #  continue    #ha kapott valami jot, menjen tovabb
    #print('best lock is ',bestLock,'ennek ',melyikJatekos)
    return bestLock

def select_button(tabla,i,j):
    global hanyadikLepes,kiJon,mitCsinal, elozoCellaZold, elozoCellaPiros
    hanyadikLepes += 1
    eredmeny = 0
    
   
    if (mitCsinal %2 == 1):    #lep
        button[i][j].config(bg="green")
        tabla[i][j] = 3
        button[elozoCellaZold[0]][elozoCellaZold[1]].config(bg="white")
        tabla[elozoCellaZold[0]][elozoCellaZold[1]] = 0

        #print(np.matrix(tabla))
        
        eredmeny=allas(tabla,3)
        if eredmeny == 1:
            whosNext.grid(row=4,column=0)
            whosNext.config(width=10, height=5)
            whosNext["text"]="piros nyert"
        else:
            if eredmeny == -1:
                whosNext.grid(row=4,column=0)
                whosNext.config(width=10, height=5)
                whosNext["text"]="zold nyert"
            #else:
             #   print('nem nyert senki')
                #print(np.matrix(tabla))

        mitCsinal += 1
        whosNext.grid(row=4,column=0)
        whosNext.config(width=10, height=5)
        whosNext["text"]="zold kizar"
        elozoCellaZold[0] = i
        elozoCellaZold[1] = j

        eredmeny=allas(tabla,3)
        if eredmeny == 1:
            whosNext.grid(row=4,column=0)
            whosNext.config(width=10, height=5)
            whosNext["text"]="piros nyert"
        else:
            if eredmeny == -1:
                whosNext.grid(row=4,column=0)
                whosNext.config(width=10, height=5)
                whosNext["text"]="zold nyert"
            #else:
                #print('nem nyert senki')
                #print(np.matrix(tabla))
    else:
        if(mitCsinal == 2):    #kizar
            button[i][j].config(bg="black")   
            tabla[i][j] = 1
            mitCsinal = 1
            whosNext.grid(row=4,column=0)
            whosNext.config(width=10, height=5)
            whosNext["text"]="piros lep"

    #else:
     #   if (mitCsinal == 1):    #lep bot
            #time.sleep(3)
            #print(np.matrix(tabla))
            eredmeny=allas(tabla,eredmeny)
            #print('eredmeny ',2)
            if eredmeny == 1:
                whosNext.grid(row=4,column=0)
                whosNext.config(width=10, height=5)
                whosNext["text"]="piros nyert"
                #print('piros nyert')
            else:
                if eredmeny == -1:
                    whosNext.grid(row=4,column=0)
                    whosNext.config(width=10, height=5)
                    whosNext["text"]="zold nyert"
                    #print('zold nyert')
                #else:
                    #print('nem nyert senki')
                  #  print(np.matrix(tabla))
           

            lepes = [0 for i in range(0,2)]
            #time.sleep(3)
            lepes = findBestMove(tabla,2)
            #time.sleep(3)
            #print('lepes',lepes)
            if lepes != None:
                tabla[lepes[0]][lepes[1]] = 2
                button[lepes[0]][lepes[1]].config(bg="red")
                button[elozoCellaPiros[0]][elozoCellaPiros[1]].config(bg="white")
                tabla[elozoCellaPiros[0]][elozoCellaPiros[1]] = 0
                elozoCellaPiros[0] = lepes[0]
                elozoCellaPiros[1] = lepes[1]

               # print(np.matrix(tabla))
                whosNext.grid(row=4,column=0)
                whosNext.config(width=10, height=5)
                whosNext["text"]="piros kizar"
            # time.sleep(3)
                
                #kizar
                kizar=[0 for i in range(0,2)]
                kizar = findBestLock(tabla,2)
                button[kizar[0]][kizar[1]].config(bg="black")
                tabla[kizar[0]][kizar[1]] = 1
                #print(np.matrix(tabla))

                eredmeny=allas(tabla,2)
                #print('eredmeny ',eredmeny)
                if eredmeny == 1:
                    whosNext.grid(row=4,column=0)
                    whosNext.config(width=10, height=5)
                    whosNext["text"]="piros nyert"
                   #print('piros nyert')
                else:
                    if eredmeny == -1:
                        whosNext.grid(row=4,column=0)
                        whosNext.config(width=10, height=5)
                        whosNext["text"]="zold nyert"
                        #print('zold nyert')
                    #else:
                       # print('nem nyert senki')
                        #print(np.matrix(tabla))
                
                whosNext.grid(row=4,column=0)
                whosNext.config(width=10, height=5)
                whosNext["text"]="zold lep"

            #time.sleep(3)

#racs felepitese
for i in range(0,3):
    for j in range(0,3):   
        #cellak[szamlalo] = Cella(i,j,0)  
        #print("szamlalo= ",szamlalo, "i j= ",i,j,"cellak[szamlalo].x,y= ",cellak[szamlalo].x,cellak[szamlalo].y)
        button[i][j]=tkinter.Button(master, command=lambda  i=i, j=j:select_button(tabla,i,j))
        szamlalo += 1
        button[i][j].grid(row=i,column=j)
        button[i][j].config(width=10, height=5)
        button[i][j].config(bg="white")
        
#kezdo allas
piros = (0,1,2)
zold = (2,1,3)
button[0][1].config(bg="red")
button[2][1].config(bg="green")




exitButton = tkinter.Button(master, text="Exit", command=master.destroy)
exitButton.grid(row=5,column=3)
exitButton.config(width=10, height=5)

matrix = []

#kezdodik a jatek


master.mainloop()