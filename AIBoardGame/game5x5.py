#Szasz Csongor
#523/II
#scim1957


import sys
import numpy as np
from copy import copy, deepcopy
import tkinter

def keres_poz(i,j,tabla,n):
    if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:
        return False
    if i-1>=0 and tabla[i-1][j]==0:
        return False
    if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
        return False
    if j-1>=0 and tabla[i][j-1]==0:
        return False
    if j+1<n and tabla[i][j+1]==0:
        return False
    if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
        return False
    if i+1<n and tabla[i+1][j]==0:
        return False
    if i+1<n and j+1<n and tabla[i+1][j+1]==0:
        return False
    return True

def nyert_e(tabla,n,soros):
    if(soros==True):
        for i in range(n):
            for j in range(n):
                if (tabla[i][j]==2):
                    mentes_i=i
                    mentes_j=j
        if(keres_poz(mentes_i,mentes_j,tabla,n)):
            return -np.inf
        else:
            return 0
    else:
        for i in range(n):
            for j in range(n):
                if (tabla[i][j]==3):
                    mentes_i=i
                    mentes_j=j
        if(keres_poz(mentes_i,mentes_j,tabla,n)):
            return np.inf
        else:
            return 0


def tabla_ertekeles(tabla,n):
    a = 1
    for i in range(n):
        for j in range(n):
            if (tabla[i][j]==2):
                mentes_i=i
                mentes_j=j
    i = mentes_i
    j = mentes_j
    maxi = 0
    if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:
        maxi = maxi + 1
    if i-1>=0 and tabla[i-1][j]==0:
        maxi = maxi + 1
    if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
        maxi = maxi + 1
    if j-1>=0 and tabla[i][j-1]==0:
        maxi = maxi + 1
    if j+1<n and tabla[i][j+1]==0:
        maxi = maxi + 1
    if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
        maxi = maxi + 1
    if i+1<n and tabla[i+1][j]==0:
        maxi = maxi + 1
    if i+1<n and j+1<n and tabla[i+1][j+1]==0:
        maxi = maxi + 1
    
    for i in range(n):
        for j in range(n):
            if (tabla[i][j]==3):
                mentes_i=i
                mentes_j=j
    i = mentes_i
    j = mentes_j
    mini = 0
    if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0:
        mini = mini + 1
    if i-1>=0 and tabla[i-1][j]==0:
        mini = mini + 1
    if i-1>=0 and j+1<n and tabla[i-1][j+1]==0:
        mini = mini + 1
    if j-1>=0 and tabla[i][j-1]==0:
        mini = mini + 1
    if j+1<n and tabla[i][j+1]==0:
        mini = mini + 1
    if i+1<n and j-1>=0 and tabla[i+1][j-1]==0:
        mini = mini + 1
    if i+1<n and tabla[i+1][j]==0:
        mini = mini + 1
    if i+1<n and j+1<n and tabla[i+1][j+1]==0:
        mini = mini + 1

    return maxi - a*mini

def melyseg_hat(tabla,n):
    zero_szamlalo = 0
    for i in range(n):
        for j in range(n):
            if tabla[i][j] == 0:
                zero_szamlalo = zero_szamlalo+1
    if zero_szamlalo>=15:
        return 2
    if zero_szamlalo>=7:
        return 3
    if zero_szamlalo>=5:
        return 4
    return 6

def masol(tabla1,tabla2,n):
    for i in range(n):
        for j in range(n):
            tabla1[i][j] = tabla2[i][j]

class Lepes:
    def __init__(self):
        self.tabla=[[0]]
        self.ertek=-2

def fun():
    return Lepes()

def print_tabla(tabla,n):
    print("")
    for i in range(n):
        for j in range(n):
            print(tabla[i][j] , end = " ")
        print("")
    print("")

def minimax(tabla,n,melyseg,maximizingPlayer,alpha,beta):
    lepes = fun()
    #lepes.tabla=tabla
    eva = []
    palyak = []
    szamlalo=0
    max_melyseg = melyseg_hat(tabla,n)
    if maximizingPlayer:                                #A BOT jatekos a soros
        node_value = nyert_e(tabla,n,maximizingPlayer)
        if node_value!=0:
            lepes.ertek = node_value
            lepes.tabla=tabla
            #print_tabla(tabla,3)
            return lepes

        if melyseg == max_melyseg:
            lepes.ertek = tabla_ertekeles(tabla,n)/melyseg
            lepes.tabla = tabla
            return lepes
        
        for i in range(n):                  #Megkeresem a BOT aktualis poziciojat
            for j in range(n):
                if (tabla[i][j]==2):
                    mentes_i=i
                    mentes_j=j
        
        i=mentes_i
        j=mentes_j

        best_val = -np.inf

        maxi_eva = fun()
        maxi_eva.ertek = best_val
        maxi_eva.tabla = deepcopy(tabla)

        if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0 and alpha < beta:                #Kovetkezo lepes
            tabla[i][j] = 0
            tabla[i-1][j-1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i-1][j-1] = 0
        if i-1>=0 and tabla[i-1][j]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i-1][j] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i-1][j] = 0
        if i-1>=0 and j+1<n and tabla[i-1][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i-1][j+1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i-1][j+1] = 0
        if j-1>=0 and tabla[i][j-1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i][j-1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i][j-1] = 0
        if j+1<n and tabla[i][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i][j+1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i][j+1] = 0
        if i+1<n and j-1>=0 and tabla[i+1][j-1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j-1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i+1][j-1] = 0
        if i+1<n and tabla[i+1][j]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i+1][j] = 0
        if i+1<n and j+1<n and tabla[i+1][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j+1] = 2
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,False,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek >= maxi_eva.ertek:
                            #maxi_eva = lepes
                            maxi_eva.ertek = lepes.ertek
                            masol(maxi_eva.tabla,lepes.tabla,n)
                        if alpha < maxi_eva.ertek:
                            alpha = maxi_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 2
            tabla[i+1][j+1] = 0
        
        return maxi_eva

    else:                                               #Player a soros
        node_value = nyert_e(tabla,n,maximizingPlayer)
        if node_value!=0:
            lepes.ertek = node_value/melyseg
            lepes.tabla=tabla
            #print_tabla(tabla,n)
            return lepes

        if melyseg == max_melyseg:
            lepes.ertek = tabla_ertekeles(tabla,n)/melyseg
            lepes.tabla = tabla
            return lepes
        
        for i in range(n):                  #Megkeresem a Player aktualis poziciojat
            for j in range(n):
                if (tabla[i][j]==3):
                    mentes_i=i
                    mentes_j=j
        
        i=mentes_i
        j=mentes_j

        best_val = np.inf

        mini_eva = fun()
        mini_eva.ertek = best_val
        mini_eva.tabla = deepcopy(tabla)

        if i-1>=0 and j-1>=0 and tabla[i-1][j-1]==0 and alpha < beta:                #Kovetkezo lepes
            tabla[i][j] = 0
            tabla[i-1][j-1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i-1][j-1] = 0
        if i-1>=0 and tabla[i-1][j]==0:
            tabla[i][j] = 0
            tabla[i-1][j] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i-1][j] = 0
        if i-1>=0 and j+1<n and tabla[i-1][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i-1][j+1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0 
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i-1][j+1] = 0
        if j-1>=0 and tabla[i][j-1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i][j-1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i][j-1] = 0
        if j+1<n and tabla[i][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i][j+1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i][j+1] = 0
        if i+1<n and j-1>=0 and tabla[i+1][j-1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j-1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i+1][j-1] = 0
        if i+1<n and tabla[i+1][j]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i+1][j] = 0
        if i+1<n and j+1<n and tabla[i+1][j+1]==0 and alpha < beta:
            tabla[i][j] = 0
            tabla[i+1][j+1] = 3
            for k in range(n):
                if beta <= alpha:
                    break
                for l in range(n):
                    if tabla[k][l] == 0:
                        tabla[k][l] = 1
                        lepes = minimax(tabla,n,melyseg+1,True,alpha,beta)
                        #lepes.tabla = tabla
                        masol(lepes.tabla,tabla,n)
                        if lepes.ertek <= mini_eva.ertek:
                            #mini_eva = lepes
                            mini_eva.ertek = lepes.ertek
                            masol(mini_eva.tabla,lepes.tabla,n)
                        if alpha > mini_eva.ertek:
                            alpha = mini_eva.ertek
                        tabla[k][l] = 0
                        if beta <= alpha:
                            break
            tabla[i][j] = 3
            tabla[i+1][j+1] = 0

        return mini_eva


global lepes_szamlalo
lepes_szamlalo = 0


def push(i,j,tabla,n,button):
    global lepes_szamlalo
    if(tabla[i][j]!=0):
        print("Nem lehet oda lepni")
        return
    if lepes_szamlalo%2==0:
        for k in range(n):
            for l in range(n):
                if(tabla[k][l] == 3):
                    regi_i = k
                    regi_j = l
        if abs(regi_i-i)>=2 or abs(regi_j-j)>=2:
            print("Nem lehet oda lepni")
            return
        tabla[i][j] = 3
        tabla[regi_i][regi_j] = 0
        draw(tabla,button,n)
    else:
        tabla[i][j]=1
        draw(tabla,button,n)
        if nyert_e(tabla,n,True)!=0:
            print("Nyertel :)")
            return
        kov = fun()
        kov = minimax(tabla,n,0,True,-np.inf,np.inf)
        #print_tabla(kov.tabla,3)
        for v in range(n):
            for b in range(n):
                tabla[v][b]=kov.tabla[v][b]
        print(kov.ertek)
        draw(tabla,button,n)
        if nyert_e(tabla,n,False)!=0:
            print("Vesztettel :(")
            return
    #print(i)
    #print(j)
    lepes_szamlalo=lepes_szamlalo+1
    #print(lepes_szamlalo)

def draw(tabla,button,n):
    for i in range(n):
        for j in range(n):
            if tabla[i][j]==0:
                button[i][j].config(bg="white")
            elif tabla[i][j] == 1:
                button[i][j].config(bg="grey")
            elif tabla[i][j] == 2:
                button[i][j].config(bg="red")
            elif tabla[i][j] == 3:
                button[i][j].config(bg="green")



# Call main function
if __name__ == '__main__':
    tabla = [[0,0,2,0,0],
    [0,0,0,0,0],
    [0,0,0,0,0],
    [0,0,0,0,0],
    [0,0,3,0,0]]
    #mas=tabla
    n=5
    
    lepes = fun()

    bot_kezd = False

    if bot_kezd:
        tabla = [[0,0,0,0,0],[0,0,0,2,0],[0,0,0,0,0],[0,0,1,0,0],[0,0,3,0,0]]

    master=tkinter.Tk()
    master.title("Kutyaszorito")
    master.geometry("418x433")

    button = [ [ 0 for i in range(n) ] for j in range(n) ]
    for i in range(0,n):
        for j in range(0,n):   
            button[i][j]=tkinter.Button(master,command=lambda  i=i, j=j:push(i,j,tabla,n,button))
            button[i][j].grid(row=i,column=j)
            button[i][j].config(width=10, height=5)
            button[i][j].config(bg="white")

    draw(tabla,button,n)

    master.mainloop()

    #print(tabla[0][2])