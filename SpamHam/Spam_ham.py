import numpy as np

class Probaility:
    def __init__(self):
        self.word = ""
        self.ham = 0
        self.spam = 0
        self.ham001 = 0
        self.spam001 = 0
        self.ham01 = 0
        self.spam01 = 0
        self.ham1 = 0
        self.spam1 = 0
        
def fun():
    return Probaility()

class Words:
    def __init__(self):
        self.word = ""
        self.counter = 0

def fun2():
    return Words()

class Results:
    def __init__(self):
        self.result = 0
        self.result001 = 0
        self.result01 = 0
        self.result1 = 0

def fun3():
    return Results()

def approved(word, stopwords):
    if len(word) == 0:
        return False
    for i in range(len(stopwords)):
        if word == stopwords[i]:
            return False
    if word =='.' or word ==',' or word ==':' or word =='/' or word =='*' or word =='"' or word =='\'' or word =='(' or word ==')' or word =='[' or word ==']' or word =='{' or word =='}':
        return False
    return True

def insert_probability(classs, word, probability):
    if classs == 0:
        for i in range(len(probability)):
            if probability[i].word == word:
                probability[i].ham = probability[i].ham + 1
                return
        new = fun()
        new.word = word
        new.ham = 1
        new.spam = 0
        probability.append(new)
    else:
        for i in range(len(probability)):
            if probability[i].word == word:
                probability[i].spam = probability[i].spam + 1
                return
        new = fun()
        new.word = word
        new.ham = 0
        new.spam = 1
        probability.append(new)

def insert_bag_of_words(word, bag_of_words):
    for i in range(len(bag_of_words)):
        if word == bag_of_words[i].word:
            bag_of_words[i].counter = bag_of_words[i].counter + 1
            return
    new = fun2()
    new.word = word
    new.counter = 1
    bag_of_words.append(new)

def spam_or_ham(probability, title, file_ham, file_spam, stopwords):
    if len(title) == 29:
        path = "C:/Egyetem info/4. felev/AI/Hazi/H4/enron6/ham/" + title
    else:
        path = "C:/Egyetem info/4. felev/AI/Hazi/H4/enron6/spam/" + title

    bag_of_words = []
    with open(path, errors="ignore") as email:
        for email_row in email:
            email_data = email_row.strip().split(" ")
            for i in range(len(email_data)):
                if(approved(email_data[i], stopwords) == True):
                    word = email_data[i].lower()
                    insert_bag_of_words(word, bag_of_words)

    sum = 0
    sum001 = 0
    sum01 = 0
    sum1 = 0
    for i in range(len(bag_of_words)):
        for j in range(len(probability)):
            if probability[j].word == bag_of_words[i].word:
                divide = np.log(probability[j].spam) - np.log(probability[j].ham)
                divide001 = np.log(probability[j].spam001) - np.log(probability[j].ham001)
                divide01 = np.log(probability[j].spam01) - np.log(probability[j].ham01)  
                divide1 = np.log(probability[j].spam1) - np.log(probability[j].ham1)  
                break
        mul = bag_of_words[i].counter * divide
        mul001 = bag_of_words[i].counter * divide001
        mul01 = bag_of_words[i].counter * divide01
        mul1 = bag_of_words[i].counter * divide1

        sum = sum + mul
        sum001 = sum001 + mul001
        sum01 = sum01 + mul01
        sum1 = sum1 + mul1
    
    l = np.log(file_spam) - np.log(file_ham) + sum
    l001 = np.log(file_spam) - np.log(file_ham) + sum001
    l01 = np.log(file_spam) - np.log(file_ham) + sum01
    l1 = np.log(file_spam) - np.log(file_ham) + sum1

    result = fun3()
    if l > 0:
        # return 1
        result.result = 1
    else:
        result.result = 0

    if l001 > 0:
        # return 1
        result.result001 = 1
    else:
        result.result001 = 0

    if l01 > 0:
        # return 1
        result.result01 = 1
    else:
        result.result01 = 0

    if l1 > 0:
        # return 1
        result.result1 = 1
    else:
        result.result1 = 0

    return result
                            

def mistake(test_or_train, probability, file_ham, file_spam, stopwords):
    if test_or_train == 0:
        path = "C:/Egyetem info/4. felev/AI/Hazi/H4/train.txt"
        print("Train mistake: ")
    else:
        path = "C:/Egyetem info/4. felev/AI/Hazi/H4/test.txt"
        print("Test mistake: ")

    nr_ham = 0
    nr_spam = 0
    mistake_ham = 0
    mistake_spam = 0
    mistake_ham001 = 0
    mistake_spam001 = 0
    mistake_ham01 = 0
    mistake_spam01 = 0
    mistake_ham1 = 0
    mistake_spam1 = 0
    with open(path) as f:
        for row in f:
            data = row.strip().split(".txt")
            title = data[0] + ".txt"
            bad = spam_or_ham(probability, title, file_ham, file_spam, stopwords)
            if len(title) ==29:
                nr_ham = nr_ham + 1
                if bad.result == 1:
                    mistake_ham = mistake_ham + 1 

                if bad.result001 == 1:
                    mistake_ham001 = mistake_ham001 + 1

                if bad.result01 == 1:
                    mistake_ham01 = mistake_ham01 + 1

                if bad.result1 == 1:
                    mistake_ham1 = mistake_ham1 + 1
            else:
                nr_spam = nr_spam + 1
                if bad.result == 0:
                    mistake_spam = mistake_spam + 1

                if bad.result001 == 0:
                    mistake_spam001 = mistake_spam001 + 1
                
                if bad.result01 == 0:
                    mistake_spam01 = mistake_spam01 + 1

                if bad.result1 == 0:
                    mistake_spam1 = mistake_spam1 + 1

    print()
    print("Alpha = 0")
    print("Mistake:  ", (mistake_ham + mistake_spam) / (nr_ham + nr_spam))
    print("False Positive:  ", mistake_ham / nr_ham)
    print("False Negative:  ", mistake_spam / nr_spam)

    print()
    print("Alpha = 0.01")
    print("Mistake:  ", (mistake_ham001 + mistake_spam001) / (nr_ham + nr_spam))
    print("False Positive:  ", mistake_ham001 / nr_ham)
    print("False Negative:  ", mistake_spam001 / nr_spam)

    print()
    print("Alpha = 0.1")
    print("Mistake:  ", (mistake_ham01 + mistake_spam01) / (nr_ham + nr_spam))
    print("False Positive:  ", mistake_ham01 / nr_ham)
    print("False Negative:  ", mistake_spam01 / nr_spam)

    print()
    print("Alpha = 1")
    print("Mistake:  ", (mistake_ham1 + mistake_spam1) / (nr_ham + nr_spam))
    print("False Negative:  ", mistake_ham1 / nr_ham)
    print("False Positive:  ", mistake_spam1 / nr_spam)



if __name__ == '__main__':
    stopwords = []
    i=0
    with open("C:/Egyetem info/4. felev/AI/Hazi/H4/stopwords.txt") as stopwords1:
        for row in stopwords1:
                row = row.strip().split("\n")
                stopwords.append(row[0])
    with open("C:/Egyetem info/4. felev/AI/Hazi/H4/stopwords2.txt") as stopwords2:
        for row in stopwords2:
            row = row.strip().split("\n")
            stopwords.append(row[0])

    probability = []
    nr_ham = 0
    nr_spam = 0
    file_ham = 0
    file_spam = 0
    with open("C:/Egyetem info/4. felev/AI/Hazi/H4/train.txt") as f:
        for row in f:
            data = row.strip().split(".txt")
            title = data[0] + ".txt"
            # print(len(title))
            if len(title) == 29:
                path = "C:/Egyetem info/4. felev/AI/Hazi/H4/enron6/ham/" + title
                file_ham = file_ham + 1
                with open(path) as email:
                    for email_row in email:
                        email_data = email_row.strip().split(" ")
                        for i in range(len(email_data)):
                            if(approved(email_data[i], stopwords) == True):
                                word = email_data[i].lower()
                                insert_probability(0, word, probability)
                                nr_ham = nr_ham + 1
            else:
                path = "C:/Egyetem info/4. felev/AI/Hazi/H4/enron6/spam/" + title
                file_spam = file_spam + 1
                with open(path, errors="ignore") as email2:
                    for email_row2 in email2:
                        email_data2 = email_row2.strip().split(" ")
                        for i in range(len(email_data2)):
                            if(approved(email_data2[i], stopwords) == True):
                                word = email_data2[i].lower()
                                insert_probability(1, word, probability)
                                nr_spam = nr_spam + 1
    
    sum = file_ham + file_spam
    file_ham = file_ham / sum
    file_spam = file_spam / sum
    lamda = 0.00000001
    for i in range(len(probability)):
        probability[i].ham001 = probability[i].ham + 0.01
        probability[i].ham01 = probability[i].ham + 0.1
        probability[i].ham1 = probability[i].ham + 1
        probability[i].spam001 = probability[i].spam + 0.01
        probability[i].spam01 = probability[i].spam + 0.1
        probability[i].spam1 = probability[i].spam + 1
        
        if probability[i].ham/nr_ham < lamda:
            probability[i].ham = lamda
        else:
            probability[i].ham = probability[i].ham / nr_ham

        if probability[i].spam/nr_spam < lamda:
            probability[i].spam = lamda
        else:
            probability[i].spam = probability[i].spam / nr_spam

        
        # alfa = 0.01
        if probability[i].ham001 / (nr_ham + (0.01 * len(probability))) < lamda:
            probability[i].ham001 = lamda
        else:
            probability[i].ham001 = probability[i].ham001 / (nr_ham + (0.01 * len(probability)))

        if probability[i].spam001 / (nr_spam + (0.01 * len(probability))) < lamda:
            probability[i].spam001 = lamda
        else:
            probability[i].spam001 = probability[i].spam001 / (nr_spam + (0.01 * len(probability)))

        # alfa = 0.1
        if probability[i].ham01 / (nr_ham + (0.1 * len(probability))) < lamda:
            probability[i].ham01 = lamda
        else:
            probability[i].ham01 = probability[i].ham01 / (nr_ham + (0.1 * len(probability)))

        if probability[i].spam01 / (nr_spam + (0.1 * len(probability))) < lamda:
            probability[i].spam01 = lamda
        else:
            probability[i].spam01 = probability[i].spam01 / (nr_spam + (0.1 * len(probability)))

        # alfa = 1
        if probability[i].ham1 / (nr_ham + (1 * len(probability))) < lamda:
            probability[i].ham1 = lamda
        else:
            probability[i].ham1 = probability[i].ham1 / (nr_ham + (1 * len(probability)))

        if probability[i].spam1 / (nr_spam + (1 * len(probability))) < lamda:
            probability[i].spam1 = lamda
        else:
            probability[i].spam1 = probability[i].spam1 / (nr_spam + (1 * len(probability)))

        



    # print(spam_or_ham(probability, "0082.2000-06-19.lokay.ham.txt", file_ham, file_spam, stopwords))
    # print(spam_or_ham(probability, "0101.2000-06-20.lokay.ham.txt", file_ham, file_spam, stopwords))
    # print(spam_or_ham(probability, "0125.2000-06-21.lokay.ham.txt", file_ham, file_spam, stopwords))
    # print(spam_or_ham(probability, "0002.2004-08-01.BG.spam.txt", file_ham, file_spam, stopwords))
    # print(spam_or_ham(probability, "0003.2004-08-01.BG.spam.txt", file_ham, file_spam, stopwords))
    # print(spam_or_ham(probability, "0004.2004-08-01.BG.spam.txt", file_ham, file_spam, stopwords))

    mistake(0, probability, file_ham, file_spam, stopwords)
    print()
    print()
    mistake(1, probability, file_ham, file_spam, stopwords)





